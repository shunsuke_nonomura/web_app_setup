# 概要
- ウェブアプリ作成用のサンプル
- ライブラリ、ソフトウェア等は多めに積む方針。適宜削除して使う。
- 開発中のため不要ファイルを含む

# 起動方法
- run_dev.sh : 開発環境の起動
- run_test.sh : バックエンドのテスト
- run_sphinx.sh : バックエンドのドキュメンテーション（予定）
- run_build_release.sh : フロントエンドのビルド＋リリース環境の起動
- run_release.sh : リリース環境の起動
- run_build.sh : フロントエンドのビルド

# 設定ポート
- dev
  - 8000(django)
  - 8080(vue)
- release
  - 80(nginx)

# 採用アーキテクト
- バックエンド
  - DDD : domain driven desing
    - オニオンアーキテクチャ
  - CQS：command query separation (予定)
    - CQRSにするかも
- フロントエンド
  - atomic design (予定)

# 主な構成
- バックエンド : django
- バックエンドテスト：pytest
- フロントエンド : vue
- webサーバー : nginx
- キャッシュサーバー : redis
- タスク処理 : celery
- タスク監視 : flower
- バックエンドドキュメンテーション : sphinx（予定）

# バージョン
## 0.1.0
- ベースライン作成（django + vue + nginx + redis + celery + flower）
- djangoバックエンドをdddで構成
- vueのコードは簡易作成
- nginxでリリース時にdjangoのuwisgとvueのバンドルファイルに接続するよう設定
  - 永続化領域をバックエンドのプロセスごとにオンメモリで抱えているので、複数プロセスで稼働させると不具合が生じる
  - sqlite組み込むかも
- pytestでのIFレイヤテストコードを一部実装
- 以下は実装途中のためコメントアウト中
  - redisでタスクをキャッシュするように設定
  - celeryでバックエンドハートビートを定期実行
  - flowerでredisのキャッシュデータを監視するよう設定
