// import 'material-design-icons-iconfont/dist/material-design-icons.css';
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader

import Vue from 'vue';
import Vuetify from 'vuetify/lib';
// import colors from 'vuetify/lib/util/colors';

Vue.use(Vuetify);

export default new Vuetify({
    // icons:{
    //     iconfont: 'mdi'
    // },
    theme: {
      themes: {
        dark: {
          primary: "#384E51", // ヘッダやフッタ
          // secondary: "#8bc34a",
          // accent: "#cddc39",
          // error: "#ffeb3b",
          // warning: "#ffc107",
          // info: "#ff5722",
          // success: "#795548",
        },
        light: {
          primary: "#384E51", // ヘッダやフッタ
          secondary: '#e0f2f1', // カードやリンカ
          // accent: "#cddc39",
          // error: "#ffeb3b",
          // warning: "#ffc107",
          // info: "#ff5722",
          // success: "#795548",
        },
      },
    },
            
    //         accent: '#FDF8E5',
    //         error: '#f00',

    //         tertiary: colors.pink.base,
    //         base: '#161D33',
    //         background: colors.indigo.lighten5,
    //       },

    //       light: {
    //         primary: '#e0f2f1', // カードやリンカ
    //         secondary: '#384E51', // ヘッダやフッタ
    //         base: '#161D33',
    //         header: '#384E51',
    //         subheader: '#137C84',

    //         background: '#F00',
    //       },
    //     },
    //   },
});
