/* eslint-disable */
import {Module, VuexModule, Mutation, Action, getModule} from 'vuex-module-decorators';
import { Vue } from 'vue-property-decorator';
import store from '@/store';
import { requestMethod, requestAPI } from '@/middlewares/request';

export interface ITest {

}

@Module({ dynamic: true, store, name:'test', namespaced: true})
class Test extends VuexModule implements ITest {
    @Mutation
    private updateNone(item: any){
    }
    @Action({commit:'updateNone'})
    public async onTest(parameters: any){
        // alert(0);
        Vue.set(store.state, 'testData', '')
        await requestAPI(requestMethod.GET, 'http://localhost/sandbox/', {});
        // await requestAPI(requestMethod.DELETE, '', {});
        Vue.set(store.state, 'testData', 'done')
        return { }
    }
}

const TestModule = getModule(Test);

export {TestModule}