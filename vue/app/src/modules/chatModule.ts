/* eslint-disable */
import {Module, VuexModule, Mutation, Action, getModule} from 'vuex-module-decorators';
import { Vue } from 'vue-property-decorator';
import store from '@/store';

import * as chat from '@/middlewares/chat';

export interface IChatModule {
    chatList: chat.Chat[];
}
@Module({ dynamic: true, store, name:'chat', namespaced: true})
class ChatModule extends VuexModule implements IChatModule {
    public chatList: chat.Chat[] = []

    @Mutation
    private updateNone(item: any){
    }
    @Action({commit:'updateNone'})
    public async onInit(parameters: any){
        const userId = 'u0'
        chat.chatRepository.searchByUserId(userId).then(res => {
            this.chatList = res;
        })
    }
}

const chatModule = getModule(ChatModule);

export {chatModule}