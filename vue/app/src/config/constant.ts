// edition
export const EDITION_NAME = 'TEST EDITION';

// page
export const PAGE_TITLE_LOGIN = 'ログイン';
export const PAGE_TITLE_ADMIN = '管理者ページ';
export const PAGE_TITLE_CHAT = 'チャットページ';

// tab
export const TAB_PROCESS_LOG = 'プロセスログ';
export const TAB_USER = 'ユーザ';
export const TAB_CHAT = 'チャット';

// dialog
export const DIALOG_TITLE_ADD_USER = 'ユーザ追加';
export const DIALOG_TITLE_DELETE_USER = 'ユーザ削除';

// label
export const BUTTON_LOGIN = 'ログイン';
export const BUTTON_ADD_USER = '管理者ページ';

// textarea
export const TEXTAREA_USER_ID = 'USER ID';
export const TEXTAREA_PASSWORD = 'PASSWORD';

// table column
export const COLUMN_ACTIONS = '操作';
export const COLUMN_CREATED_DATETIME = '作成時刻';
export const COLUMN_UPDATE_DATETIME = '更新時刻';

export const COLUMN_PROCESS_LOG_ID = 'プロセスログID';
export const COLUMN_PROCESS_LOG_PROCESS_NAME = 'プロセス名';
export const COLUMN_PROCESS_LOG_RESPONSE_CODE = 'レスポンスコード';
export const COLUMN_PROCESS_LOG_ERROR_MESSAGE = 'エラーメッセージ';
export const COLUMN_PROCESS_LOG_DULATION_TIMEDELTA = '処理時間(sec)';

export const COLUMN_USER_ID = 'ユーザID';
export const COLUMN_USER_ID_LIST = 'ユーザIDリスト';
export const COLUMN_USER_NAME = 'ユーザ名';
export const COLUMN_USER_NAME_LIST = 'ユーザ名リスト';
export const COLUMN_USER_ROLE = 'ユーザロール';

export const COLUMN_CHAT_ID = 'チャットID';
export const COLUMN_CHAT_TITLE = 'チャットタイトル';