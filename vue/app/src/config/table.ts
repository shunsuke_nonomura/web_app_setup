import * as TEXT from '@/config/constant';

// head
export const HEADER_PROCESS_LOG_SUMMARY = [
    {
        text: TEXT.COLUMN_CREATED_DATETIME, 
        value: 'processLogCreatedDatetime'
    },
    {
        text: TEXT.COLUMN_PROCESS_LOG_ID, 
        value: 'processLogId'
    },
    {
        text: TEXT.COLUMN_PROCESS_LOG_PROCESS_NAME, 
        value: 'processLogResponse.head.processName'
    },
    {
        text: TEXT.COLUMN_PROCESS_LOG_RESPONSE_CODE, 
        value: 'processLogResponse.head.responseCode'
    },
    {
        text: TEXT.COLUMN_PROCESS_LOG_ERROR_MESSAGE, 
        value: 'processLogResponse.head.errorMessage'
    },
    {
        text: TEXT.COLUMN_PROCESS_LOG_DULATION_TIMEDELTA, 
        value: 'processLogResponse.head.processDulationTimedelta'
    },
    {
        text: TEXT.COLUMN_ACTIONS, 
        value: 'actions',
        sortable: false
    },
]
export const HEADER_USER = [
    {
        text: TEXT.COLUMN_CREATED_DATETIME, 
        value: 'userCreatedDatetime'
    },
    {
        text: TEXT.COLUMN_USER_ID, 
        value: 'userId'
    },
    {
        text: TEXT.COLUMN_USER_NAME, 
        value: 'userName'
    },
    {
        text: TEXT.COLUMN_USER_ROLE, 
        value: 'userRole'
    },
    {
        text: TEXT.COLUMN_ACTIONS, 
        value: 'actions',
        sortable: false
    },
]
export const HEADER_TUSER = [
    {
        text: TEXT.COLUMN_USER_ID, 
        value: 'userId'
    },
    {
        text: TEXT.COLUMN_USER_NAME, 
        value: 'userName'
    },
]
export const HEADER_CHAT = [
    {
        text: TEXT.COLUMN_CHAT_ID, 
        value: 'chatId',
    },
    {
        text: TEXT.COLUMN_CHAT_TITLE, 
        value: 'chatTitle',
    },
    {
        text: TEXT.COLUMN_USER_ID_LIST, 
        value: 'chatUserIdList',
    },
    {
        text: TEXT.COLUMN_CREATED_DATETIME, 
        value: 'chatCreatedDatetime',
    },
    {
        text: TEXT.COLUMN_UPDATE_DATETIME, 
        value: 'chatUpdateDatetime',
    },
    {
        text: TEXT.COLUMN_ACTIONS, 
        value: 'actions',
        sortable: false
    },
]