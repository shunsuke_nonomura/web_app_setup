import axios from 'axios';
// import store from '@/store';

enum requestMethod {
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE',
}

// eslint-disable-next-line
async function requestAPI(method: requestMethod, url: string, parameter: any) {
    // const timeStartLocal = new Date();
    // const timeEndLocal = new Date();
    // const timeDiff = timeEndLocal.getTime() - timeStartLocal.getTime();

    if (method === requestMethod.GET) {
        try {
            const response = await axios.get(url, {params: parameter, timeout : -1});
            return response.data;
        } catch (error) {
            alert(error);
            return {};
        }
    }
    if (method === requestMethod.DELETE) {
        try {
            const response = await axios.delete(url, {data: parameter});
            return response.data;
        } catch (error) {
            alert(error);
            return {};
        }
    }
    if (method === requestMethod.POST) {
        try {
            const response = await axios.post(url, parameter);
            return response.data;
        } catch (error) {
            alert(error);
            return {};
        }
    }
    if (method === requestMethod.PUT) {
        try { 
            const response = await axios.put(url, parameter);
            return response.data;
        } catch (error) {
            alert(error);
            return {};
        }
    }
    alert('Error: method error')
    return {}
}

export { requestMethod, requestAPI }