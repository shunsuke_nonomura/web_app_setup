const backendRoot = process.env.VUE_APP_BACKEND_ROOT
import { toCamel } from 'snake-camel';
import axios from 'axios';

export interface UserProps{
    userId: string;
    userPassword: string;
    userName: string;
    userRole: string;
    userCreatedDatetime: string;
    // userMailAddress: string;
}

export class User {
    private _userId: string;
    private _userPassword: string;
    private _userName: string;
    private _userRole: string;
    private _userCreatedDatetime: string;

    get userId() {
        return this._userId
    }
    set userId(v: string) {
        this._userId = v;
    }
    get userName() {
        return this._userName
    }
    set userName(v: string) {
        this._userName = v;
    }
    get userRole() {
        return this._userRole
    }
    get userCreatedDatetime() {
        return this._userCreatedDatetime
    }

    // コストラクタ
    constructor(props: UserProps) {
        this._userId = props.userId;
        this._userPassword = props.userPassword;
        this._userName = props.userName;
        this._userRole = props.userRole;
        this._userCreatedDatetime = props.userCreatedDatetime;
    }
    // 認証
    public auth(): Promise<boolean> {
        // ユーザ名がない場合そもそもリクエストできない
        // バックエンドに認証を投げる
        const params = {
            'user_password': this._userPassword,
        }
        return axios.post(`${backendRoot}/user/${this.userId}/_login`, params).then(res => {
            const isAuth = res.data.head.response_code.charAt(0) == 'S'
            return isAuth
        })
    }
}


export class UserRepository {
    public findByUserId(userId: string): Promise<User> {
        return axios.get(`${backendRoot}/user/${userId}`).then(res => {
            const data = res.data.data.user;
            return new User(toCamel(data) as UserProps );
        })
    }
    public search(): Promise<User[]> {
        return axios.get(`${backendRoot}/user/_search`).then(res => {
            const dataList = res.data.data.user_list;
            return dataList.map((data: any) => new User(toCamel(data) as UserProps ));
        })
    }
    public delete(userId: string): Promise<User> {
        return axios.delete(`${backendRoot}/user/${userId}`).then(res => {
            const data = res.data.data;
            return data;
        })
    }

    public register(user: User): Promise<boolean>{
        const params = {
            "user": {
                "user_id": user.userId,
                "user_name": user.userName,
            }
        }
        return axios.post(`${backendRoot}/user`, params).then(() => {
            return true
            // const data = res.data.data;
            // return data;
        })
    }

    // public append(user: User): Promise<any> {
    //     return axios.get(`${backendRoot}/user/_search`).then(res => {
    //     // return axios.get('http://127.0.0.1/view/user/_search').then(res => {
    //         const userList = res.data.data.user_list;
    //         return userList.map((user: any) => new User(toCamel(user) as UserProps ));
    //     })
    // }
}

export const userRepository = new UserRepository()