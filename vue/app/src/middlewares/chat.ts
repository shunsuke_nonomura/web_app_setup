export interface ChatProps{
    chatId: string;
    chatTitle: string;
    chatUserIdList: string[];
    chatCreatedDatetime: string;
    chatUpdateDatetime: string;
}
export class Chat {
    private _chatId: string;
    private _chatTitle: string;
    private _chatUserIdList: string[];
    private _chatCreatedDatetime: string;
    private _chatUpdateDatetime: string;

    get chatId() {
        return this._chatId
    }
    get chatTitle() {
        return this._chatTitle
    }
    set chatTitle(v: string) {
        this._chatTitle = v;
    }
    get chatUserIdList() {
        return this._chatUserIdList;
    }
    set chatUserIdList(v: string[]) {
        this._chatUserIdList = v;
    }
    
    get chatCreatedDatetime() {
        return this._chatCreatedDatetime
    }

    get chatUpdateDatetime() {
        return this._chatUpdateDatetime
    }

    // コストラクタ
    constructor(props: ChatProps) {
        this._chatId = props.chatId;
        this._chatTitle = props.chatTitle;
        this._chatUserIdList = props.chatUserIdList;
        this._chatCreatedDatetime = props.chatCreatedDatetime;
        this._chatUpdateDatetime = props.chatUpdateDatetime;
    }
}
export interface ChatMessageProps{
    chatMessageId: string;
    chatMessageData: any;
    chatMessageUserId: string;
    chatMessageCreatedDatetime: string;
    chatId: string;
}
export class ChatMessage {
    private _chatMessageId: string;
    private _chatMessageData: any;
    private _chatMessageUserId: string;
    private _chatMessageCreatedDatetime: string;
    private _chatId: string;

    get chatMessageId() {
        return this._chatMessageId
    }
    get chatMessageData() {
        return this._chatMessageData
    }
    get chatMessageDataText() {
        return JSON.stringify(this._chatMessageData)
    }
    get chatMessageUserId() {
        return this._chatMessageUserId
    }
    get chatMessageCreatedDatetime() {
        return this._chatMessageCreatedDatetime
    }
    get chatId() {
        return this._chatId
    }
    // コストラクタ
    constructor(props: ChatMessageProps) {
        this._chatMessageId = props.chatMessageId;
        this._chatMessageData = props.chatMessageData;
        this._chatMessageUserId = props.chatMessageUserId;
        this._chatMessageCreatedDatetime = props.chatMessageCreatedDatetime;
        this._chatId = props.chatId;
    }
}
export interface ChatAccessProps{
    chatAccessId: string;
    chatAccessUserId: string;
    chatAccessCreatedDatetime: string;
    chatAccessUpdateDatetime: string;
}
export class ChatAccess {
    private _chatAccessId: string;
    private _chatAccessUserId: string;
    private _chatAccessCreatedDatetime: string;
    private _chatAccessUpdateDatetime: string;

    get chatAccessId() {
        return this._chatAccessId
    }
    get chatAccessUserId() {
        return this._chatAccessUserId
    }
    get chatAccessCreatedDatetime() {
        return this._chatAccessCreatedDatetime
    }
    get chatAccessUpdateDatetime() {
        return this._chatAccessUpdateDatetime
    }
    // コストラクタ
    constructor(props: ChatAccessProps) {
        this._chatAccessId = props.chatAccessId;
        this._chatAccessUserId = props.chatAccessUserId;
        this._chatAccessCreatedDatetime = props.chatAccessCreatedDatetime;
        this._chatAccessUpdateDatetime = props.chatAccessUpdateDatetime;
    }
}



// const backendRoot = 'http://127.0.0.1:8000/view'
// const backendRoot = 'http://192.168.10.10:8000/view'
const backendRoot = process.env.VUE_APP_BACKEND_ROOT

// const userId = 'u0'
// const chatId = 'XXX'

import { toCamel, toSnake } from 'snake-camel';
import axios from 'axios';

export class ChatRepository {
    public searchByUserId(userId: string): Promise<Chat[]> {
        return axios.get(`${backendRoot}/chat/_search/_user/${userId}`).then(res => {
            const dataList = res.data.data.chat_list;
            return dataList.map((data: any) => new Chat(toCamel(data) as ChatProps ));
        })
    }

    public postChat(chat: Chat) {
        const params = {
            'chat': {
                'chat_title': chat.chatTitle,
                'chat_user_id_list': ['u0','u1'],
            },
        }
        return axios.post(`${backendRoot}/chat`, params).then(() => {
            return true;
        })
    }
}
export const chatRepository = new ChatRepository()

export class ChatMessageRepository {
    public searchByChatId(chatId: string): Promise<ChatMessage[]> {
        return axios.get(`${backendRoot}/chat_message/_search/_chat/${chatId}`).then(res => {
            const dataList = res.data.data.chat_message_list;
            return dataList.map((data: any) => new ChatMessage(toCamel(data) as ChatMessageProps ));
        })
    }

    public postChatMessage(chatMessage: ChatMessage) {
        const params = {
            'chat_message': {
                'chat_message_data': toSnake(chatMessage.chatMessageData),
                'chat_message_user_id': chatMessage.chatMessageUserId,
                'chat_id': chatMessage.chatId,
            },
        }
        // toSnake(chatMessage)
        // console.log(params)
        return axios.post(`${backendRoot}/chat_message`, params).then(() => {
            return true;
            // const dataList = res.data.data.chat_message_list;
            // return dataList.map((data: any) => new ChatMessage(toCamel(data) as ChatMessageProps ));
        })
    }
}
export const chatMessageRepository = new ChatMessageRepository()

export class ChatAccessRepository {
    public search(): Promise<ChatAccess[]> {
        return axios.get(`${backendRoot}/chat_access/_search`).then(res => {
            const dataList = res.data.data.chat_access_list;
            return dataList.map((data: any) => new ChatAccess(toCamel(data) as ChatAccessProps ));
        })
    }
    // public update(): Promise<ChatAccess[]> {
    //     return axios.get(`${backendRoot}/chat_access/_search`).then(res => {
    //         const dataList = res.data.data.chat_access_list;
    //         return dataList.map((data: any) => new ChatAccess(toCamel(data) as ChatAccessProps ));
    //     })
    // }
}
export const chatAccessRepository = new ChatAccessRepository()