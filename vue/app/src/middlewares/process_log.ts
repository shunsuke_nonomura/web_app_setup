export interface ProcessLogProps{
    processLogId: string;
    processLogResponse: any;
    processLogCreatedDatetime: string;
}
export class ProcessLog {
    private _processLogId: string;
    private _processLogResponse: any;
    private _processLogCreatedDatetime: string;

    get processLogId() {
        return this._processLogId
    }
    get processLogResponse() {
        return this._processLogResponse
    }
    get processLogCreatedDatetime() {
        return this._processLogCreatedDatetime
    }

    // コストラクタ
    constructor(props: ProcessLogProps) {
        this._processLogId = props.processLogId;
        this._processLogResponse = props.processLogResponse;
        this._processLogCreatedDatetime = props.processLogCreatedDatetime;
    }
}

// export default interface UserRepository {
  //   findAll(): Promise<User[]>;
  // }

// const backendRoot = 'http://127.0.0.1:8000/view'
// const backendRoot = 'http://192.168.10.10:8000/view'
const backendRoot = process.env.VUE_APP_BACKEND_ROOT

import { toCamel } from 'snake-camel';
import axios from 'axios';
export class ProcessLogRepository {
    public search(): Promise<ProcessLog[]> {
        // return axios.get('http://192.168.10.10:8000/view/user/_search').then(res => {
        return axios.get(`${backendRoot}/process_log/_search`).then(res => {
        // return axios.get('http://127.0.0.1/view/user/_search').then(res => {
            const dataList = res.data.data.process_log_list;
            return dataList.map((data: any) => new ProcessLog(toCamel(data) as ProcessLogProps ));
        })
    }
}

export const processLogRepository = new ProcessLogRepository()