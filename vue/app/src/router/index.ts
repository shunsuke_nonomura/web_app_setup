import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const pageTop = 'salestech';
const routes = [
  // ログインページ //////////////////////////////
  {
    path: '/',
    redirect: '/login',
  },
  {
    path: '/login',
    component: () => import( '@/components/pages/Login.vue'),
  },

  // 管理者 //////////////////////////////
  {
    path: '/admin',
    component: () => import( '@/components/pages/Admin.vue'),
  },

  // チャット //////////////////////////////
  {
    path: '/chat',
    component: () => import( '@/components/pages/Chat.vue'),
  },


  // セールステックページ //////////////////////////////
  {
      path: '/' + pageTop,
      redirect: '/' + pageTop + '/dashboard',
      component: () => import('@/components/pages/Salestech.vue'),
    //   children:[
    //     {
    //         path: '/' + pageTop + '/sandbox',
    //         component: () => import('@/components/salestechSubPages/Sandbox.vue'),
    //     }
    //   ]
  },

  // 何もないとき
  {
      path: '/*',
      component: () => import('@/components/pages/404.vue'),
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
