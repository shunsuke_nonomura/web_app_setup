import  Vue from 'vue';
import  Vuex from 'vuex';
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

const initalState = {
    version: 'v0.1.0',
    userId: 'u1',
};

export default new Vuex.Store({
    state: initalState,
    mutations: {

    },
    actions:{

    },
    plugins:[createPersistedState()], // ローカルストレージのステート保持
})