export interface UserProps{
    userId: string;
    userPassword: string;
    userName: string;
    userRole: string;
    userCreatedDatetime: string;
}
export class User {
    private _userId: string;
    private _userPassword: string;
    private _userName: string;
    private _userRole: string;
    private _userCreatedDatetime: string;

    get userId() {
        return this._userId
    }
    set userId(v: string) {
        this._userId = v;
    }
    get userName() {
        return this._userName
    }
    set userName(v: string) {
        this._userName = v;
    }
    get userRole() {
        return this._userRole
    }
    get userCreatedDatetime() {
        return this._userCreatedDatetime
    }

    // コストラクタ
    constructor(props: UserProps) {
        this._userId = props.userId;
        this._userPassword = props.userPassword;
        this._userName = props.userName;
        this._userRole = props.userRole;
        this._userCreatedDatetime = props.userCreatedDatetime;
    }
    // 認証
    public auth() { // インフラレイヤで変わりそう・・・
        // バックエンドに認証を投げる
        return false
    }
}