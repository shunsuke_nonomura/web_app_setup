export interface ProcessLogProps{
    processLogId: string;
    processLogResponse: any;
    processLogCreatedDatetime: string;
}
export class ProcessLog {
    private _processLogId: string;
    private _processLogResponse: any;
    private _processLogCreatedDatetime: string;

    get processLogId() {
        return this._processLogId
    }
    get processLogResponse() {
        return this._processLogResponse
    }
    get processLogCreatedDatetime() {
        return this._processLogCreatedDatetime
    }

    // コストラクタ
    constructor(props: ProcessLogProps) {
        this._processLogId = props.processLogId;
        this._processLogResponse = props.processLogResponse;
        this._processLogCreatedDatetime = props.processLogCreatedDatetime;
    }
}