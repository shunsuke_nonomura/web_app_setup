# プロジェクト作成
django-admin startproject [mysite]

# アプリ作成
python manage.py startapp [polls]

# サーバー起動
python manage.py runserver 0.0.0.0:8000

# wsgiサーバー起動
uwsgi --http :8000 --module config.wsgi
