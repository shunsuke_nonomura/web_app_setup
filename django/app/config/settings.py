"""
Django settings for config project.

Generated by 'django-admin startproject' using Django 2.1.7.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.1/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'jkv=ke2-ks714)*2*nh9df^)by&j(^2^ni07l4uj#6avgf)tr0'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# ALLOWED_HOSTS = []
ALLOWED_HOSTS = ['*'] # 開発用


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',# rest api
    'corsheaders',# cors用

    # 'huey.contrib.djhuey',  # Add this to the list.
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware',#cors用
]

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

# LANGUAGE_CODE = 'en-us'
LANGUAGE_CODE = 'ja' # 日本語

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/static/'

# CORS 制御設定
# !!! XSS要因になるので本番環境では停止 !!!
CORS_ORIGIN_ALLOW_ALL = True

# # HUEY
# HUEY = {
#     'huey_class': 'huey.RedisHuey',  # Huey implementation to use.
#     # 'name': settings.DATABASES['default']['NAME'],  # Use db name for huey.
#     'name': DATABASES['default']['NAME'],  # Use db name for huey.
#     'results': True,  # Store return values of tasks.
#     'store_none': False,  # If a task returns None, do not save to results.
#     # 'immediate': settings.DEBUG,  # If DEBUG=True, run synchronously.
#     'immediate': False,  # If DEBUG=True, run synchronously.
#     'utc': True,  # Use UTC for all times internally.
#     'blocking': True,  # Perform blocking pop rather than poll Redis.
#     'connection': {
#         # 'host': 'localhost',
#         'host': 'redis',
#         'port': 6379,
#         'db': 0,
#         'connection_pool': None,  # Definitely you should use pooling!
#         # ... tons of other options, see redis-py for details.

#         # huey-specific connection parameters.
#         'read_timeout': 1,  # If not polling (blocking pop), use timeout.
#         'url': None,  # Allow Redis config via a DSN.
#     },
#     'consumer': {
#         'workers': 1,
#         'worker_type': 'thread',
#         'initial_delay': 0.1,  # Smallest polling interval, same as -d.
#         'backoff': 1.15,  # Exponential backoff using this rate, -b.
#         'max_delay': 10.0,  # Max possible polling interval, -m.
#         'scheduler_interval': 1,  # Check schedule every second, -s.
#         'periodic': True,  # Enable crontab feature.
#         'check_worker_health': True,  # Enable worker health checks.
#         'health_check_interval': 1,  # Check worker health every second.
#     },
# }

# DIP
import inject
from application_core import *
from infrastructure import *

def my_config(binder):
    # core
    binder.bind(ResponseFormBuilder,ResponseFormBuilderImplement())
    binder.bind(TaskManagementService,TaskManagementServiceImplement())
    binder.bind(TransferMessageService,TransferMessageServiceConsole())

    # system
    binder.bind(ProcessLogRepository,MemoryProcessLogRepository())
    binder.bind(ProcessLogFactory,MemoryProcessLogFactory())

    # user
    binder.bind(UserRepository,MemoryUserRepository())
    binder.bind(UserFactory,MemoryUserFactory())

    # chat
    binder.bind(ChatRepository,MemoryChatRepository())
    binder.bind(ChatFactory,MemoryChatFactory())
    binder.bind(ChatMessageRepository,MemoryChatMessageRepository())
    binder.bind(ChatMessageFactory,MemoryChatMessageFactory())
    binder.bind(ChatAccessRepository,MemoryChatAccessRepository())

inject.configure(my_config)

# # sqlite reset
# sqlite_driver = SqliteDriver()
# sqlite_driver.reset_db()