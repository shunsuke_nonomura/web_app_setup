from application_core import *
import json

def test_user_domain_model(client):
    assert UserId(__root__='a') == UserId(__root__='a')
    assert UserId(__root__='a') != UserId(__root__='b')
    user_a1 = User.parse_obj(
        {
            'user_id': 'a',
            'user_password': 'a',
            'user_name': 'a',
            'user_role': UserRoleEnum.General,
            'user_created_datetime': datetime.now()
        }
    )
    user_a2 = User.parse_obj(
        {
            'user_id': 'a',
            'user_password': 'a',
            'user_name': 'a',
            'user_role': UserRoleEnum.General,
            'user_created_datetime': datetime.now()
        }
    )
    user_b1 = User.parse_obj(
        {
            'user_id': 'b',
            'user_password': 'a',
            'user_name': 'a',
            'user_role': UserRoleEnum.General,
            'user_created_datetime': datetime.now()
        }
    )
    assert user_a1 == user_a1
    assert user_a1 != user_b1
    assert user_a1 == user_a2

def test_user_normal(client):
    # ユーザ取得が正しい
    user_id = "u0" # 存在するユーザ
    response = client.get(
        path=f"/view/user/{user_id}"
    )
    content = json.loads(response.content)
    assert content["head"]["process_code"] == ProcessCodeEnum.UserFindByUserId
    assert content["head"]["error_message"] == ""
    assert content["data"]["user"]["user_id"] == user_id

    # ユーザ削除が正しい
    response = client.delete(
        path=f"/view/user/{user_id}"
    )
    content = json.loads(response.content)
    assert content["head"]["process_code"] == ProcessCodeEnum.UserDelete
    assert content["head"]["error_message"] == ""
    response = client.get(
        path=f"/view/user/{user_id}"
    )
    # 消えたことを確認
    content = json.loads(response.content)
    assert content["head"]["process_code"] == ProcessCodeEnum.UserFindByUserId
    assert content["head"]["error_message"] == "UserNotFoundError"

    # ユーザ追加が正しい
    user_id = "hoge" # 作成するユーザ
    data = {
        "user": {
            "user_id": user_id,
            "user_name": "huga",
        }
    }
    response = client.post(
        path=f"/view/user", 
        data=json.dumps(data), 
        content_type='application/json'
    )
    content = json.loads(response.content)
    assert content["head"]["process_code"] == ProcessCodeEnum.UserSignUp
    assert content["head"]["error_message"] == ""
    # 追加されているか確認
    response = client.get(
        path=f"/view/user/{user_id}"
    )
    content = json.loads(response.content)
    assert content["head"]["process_code"] == ProcessCodeEnum.UserFindByUserId
    assert content["head"]["error_message"] == ""
    assert content["data"]["user"]["user_id"] == user_id

    # 一覧表示できる
    response = client.get(
        path=f"/view/user/_search"
    )
    content = json.loads(response.content)
    assert content["head"]["process_code"] == ProcessCodeEnum.UserSearch
    assert content["head"]["error_message"] == ""

    # ログインできる
    user_id = "test" # 作成するユーザ
    data = {
        "user_password": user_id
    }
    response = client.post(
        path=f"/view/user/{user_id}/_login", 
        data=json.dumps(data), 
        content_type='application/json'
    )
    content = json.loads(response.content)
    assert content["head"]["process_code"] == ProcessCodeEnum.UserLogin
    assert content["head"]["error_message"] == ""

    # ログインできない
    user_id = "test2" # 作成するユーザ
    data = {
        "user_password": user_id
    }
    response = client.post(
        path=f"/view/user/{user_id}/_login", 
        data=json.dumps(data), 
        content_type='application/json'
    )
    content = json.loads(response.content)
    assert content["head"]["process_code"] == ProcessCodeEnum.UserLogin
    assert content["head"]["error_message"] != ""