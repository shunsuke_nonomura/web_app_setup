from application_core import *
import json

def test_chat_normal(client):
    # チャット投函が正しい
    data = {
        "chat": {
            "chat_user_id_list": ["u0", "u1"], 
            "chat_title": "hugahuga",
        }
    }
    response = client.post(
        path=f"/view/chat",
        data=json.dumps(data), 
        content_type='application/json'
    )
    content = json.loads(response.content)
    assert content["head"]["error_message"] == ""

    # チャット取得が正しい
    # チャットメッセージ取得が正しい、投函が正しい
    user_id = "u0" # 存在するユーザ
    response = client.get( # user_idのチャット一覧取得
        path=f"/view/chat/_search/_user/{user_id}"
    )
    content = json.loads(response.content)
    assert content["head"]["error_message"] == ""
    chat_id = content["data"]["chat_list"][0]['chat_id']

    response = client.get( # chat_idのチャットメッセージ取得
        path=f"/view/chat_message/_search/_chat/{chat_id}"
    )
    content = json.loads(response.content)
    assert content["head"]["error_message"] == ""
    count_message = len(content["data"]["chat_message_list"])

    data = {
        "chat_message": {
            "chat_message_data": {
                "chat_message_data_type": "00",
                "chat_message_data_text": 'hugahuga'
            }, 
            "chat_message_user_id": "u1", 
            "chat_id": chat_id,
        }
    }
    response = client.post( # chat_idへのチャットメッセージ投函
        path=f"/view/chat_message",
        data=json.dumps(data), 
        content_type='application/json'
    )
    content = json.loads(response.content)
    assert content["head"]["error_message"] == ""

    response = client.get( # chat_idのチャットメッセージ一覧取得
        path=f"/view/chat_message/_search/_chat/{chat_id}"
    )
    content = json.loads(response.content)
    assert content["head"]["error_message"] == ""
    assert  len(content["data"]["chat_message_list"]) == count_message + 1 # 1つメッセージが増えているか確認

    # アクセスログ投函
    data = {
        'chat_access': {
            'user_id': 'test'
        }
    }
    response = client.put(
        path=f"/view/chat_access",
        data=json.dumps(data), 
        content_type='application/json'
    )
    content = json.loads(response.content)
    assert content["head"]["error_message"] == ""
    response = client.put(
        path=f"/view/chat_access",
        data=json.dumps(data), 
        content_type='application/json'
    )
    content = json.loads(response.content)
    assert content["head"]["error_message"] == ""

    # アクセスログ取得
    response = client.get(
        path=f"/view/chat_access/_search"
    )
    content = json.loads(response.content)
    assert content["head"]["error_message"] == ""