from application_core import *
import json

def test_heartbeat_normal(client):
    # ハートビート表示できる
    response = client.get(
        path=f"/view/heartbeat"
    )
    content = json.loads(response.content)
    assert content["head"]["error_message"] == ""

def test_process_log_normal(client):
    # プロセスログ表示できる
    response = client.get(
        path=f"/view/process_log/_search"
    )
    content = json.loads(response.content)
    assert content["head"]["error_message"] == ""
  