from django.shortcuts import render

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from application_core import *

from datetime import datetime
import json

class HeartbeatView(APIView, UserInterface):
    def get(self, request, *args, **kwargs):
        """
        ハートビート取得
        """
        # import hoge
        response_form = self.system_service.heartbeat()
        return Response(response_form.dict())

class TaskSearchView(APIView, UserInterface):
    def get(self, request, *args, **kwargs):
        """
        タスク取得
        """
        response_form = self.system_service.task_search()
        return Response(response_form.dict())

class ProcessLogSearchView(APIView, UserInterface):
    def get(self, request, *args, **kwargs):
        """
        処理ログ一覧取得
        """
        response_form = self.system_service.process_log_search()
        return Response(response_form.dict())

