from django.shortcuts import render

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from application_core import *

from datetime import datetime
import json

class ChatSearchUserUserIdView(APIView, UserInterface):
    def get(self, request, *args, **kwargs):
        """
        チャット一覧取得
        """
        user_id = kwargs['user_id']
        response_form = self.chat_service.chat_search_by_user_id(user_id = user_id)
        return Response(response_form.dict())

class ChatView(APIView, UserInterface):
    def post(self, request, *args, **kwargs):
        """
        チャット投函
        """
        chat_dict = request.data['chat'] if 'chat' in request.data else {}
        response_form = self.chat_service.chat_post(**chat_dict)
        return Response(response_form.dict())

class ChatMessageSearchChatChatIdView(APIView, UserInterface):
    def get(self, request, *args, **kwargs):
        """
        チャットメッセージ一覧取得
        """
        chat_id = kwargs['chat_id']
        response_form = self.chat_service.chat_message_search_by_chat_id(chat_id = chat_id)
        return Response(response_form.dict())

class ChatMessageView(APIView, UserInterface):
    def post(self, request, *args, **kwargs):
        """
        チャット投函
        """
        chat_message_dict = request.data['chat_message'] if 'chat_message' in request.data else {}
        response_form = self.chat_service.chat_message_post(**chat_message_dict)
        return Response(response_form.dict())

class ChatMessageNewFlagUserUserIdView(APIView, UserInterface):
    def get(self, request, *args, **kwargs):
        """
        チャットメッセージフラグ取得
        """
        user_id = kwargs['user_id']
        response_form = self.chat_service.chat_message_new_flag_check_by_user_id(user_id)
        return Response(response_form.dict())

class ChatAccessView(APIView, UserInterface):
    def put(self, request, *args, **kwargs):
        """
        チャットアクセス更新
        """
        chat_access_dict = request.data['chat_access'] if 'chat_access' in request.data else {}
        response_form = self.chat_service.chat_access_update_by_user_id(**chat_access_dict)
        return Response(response_form.dict())

class ChatAccessSearchView(APIView, UserInterface):
    def get(self, request, *args, **kwargs):
        """
        チャットアクセス取得
        """
        response_form = self.chat_service.chat_access_search()
        return Response(response_form.dict())