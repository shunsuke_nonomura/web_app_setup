from django.shortcuts import render

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from application_core import *

from datetime import datetime
import json

class UserView(APIView, UserInterface):
    def post(self, request, *args, **kwargs):
        """
        userの登録
        """
        user_dict = request.data['user'] if 'user' in request.data else {}
        response_form = self.user_service.sing_up(**user_dict)
        return Response(response_form.dict())

class UserIdView(APIView, UserInterface):
    def get(self, request, *args, **kwargs):
        """
        userの取得
        """
        # import hog
        user_id_str = kwargs['user_id']
        user_id = UserId(__root__=user_id_str)
        response_form = self.user_service.find_by_user_id(user_id)
        return Response(response_form.dict())

    def delete(self, request, *args, **kwargs):
        """
        userの削除
        """
        user_id = kwargs['user_id']
        response_form = self.user_service.delete(user_id)
        return Response(response_form.dict())

class UserIdLoginView(APIView, UserInterface):
    def post(self, request, *args, **kwargs):
        """
        userのログイン
        user_passwordが必要
        """
        user_id = kwargs['user_id']
        user_password = request.data['user_password'] if 'user_password' in request.data else ''
        response_form = self.user_service.login(user_id = user_id, user_password = SecretStr(user_password))
        return Response(response_form.dict())

class UserSearchView(APIView, UserInterface):
    def get(self, request, *args, **kwargs):
        """
        user一覧の取得
        """
        response_form = self.user_service.search()
        return Response(response_form.dict())
