import json

# テスト関数名は`test_`で始まる任意の名前
def test_division(client):
    # パラメータ名「client」で django.test.Client のオブジェクトを受け取る

    # POST する JSON-RPC リクエスト
    # この時点ではまだ Python の辞書型（dict）なので、「"divisor": 16,」と「"id": 1,」の
    # 各行末のカンマが許容される（JSON では不可）
    payload = {
        "jsonrpc": "2.0",
        "method": "division",
        "params": {
            "dividend": 1080,
            "divisor": 16,
        },
        "id": 1,
    }

    # JSON と下記のオプションを指定して POST リクエストを送り、API のレスポンスを得る
    response = client.get(
        path="/user/_hoge", data=payload, content_type="application/json-rpc", follow=True
    )
    content = json.loads(response.content)
    assert content["data"]["user_id"] == '_huga'

    # # HTTP レスポンスのステータスコードを期待値（200 OK）と比較する
    # assert response.status_code == 200

    # # JSON-RPC レスポンスを辞書型に変換してから結果を取り出して、それぞれの期待値と比較する
    # content = json.loads(response.content)
    # assert content["result"]["quotient"] == 67  # 商
    assert 1 == 8  # 余り