from django.apps import AppConfig

class ViewConfig(AppConfig):
    name = 'user_interface'
