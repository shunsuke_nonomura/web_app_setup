from django.urls import path

from .views import *

urlpatterns = [
    # system系
    path('heartbeat', HeartbeatView.as_view()), # 処理ログ一覧get
    path('task/_search', TaskSearchView.as_view()), # タスク一覧get
    path('process_log/_search', ProcessLogSearchView.as_view()), # 処理ログ一覧get
    # user系
    path('user', UserView.as_view()), # user登録post
    path('user/_search', UserSearchView.as_view()), # ユーザ一覧get
    path('user/<str:user_id>', UserIdView.as_view()), # ユーザ取得get、ユーザ削除delete
    path('user/<str:user_id>/_login', UserIdLoginView.as_view()), # ログインpost
    # chat系
    path('chat/_search/_user/<str:user_id>', ChatSearchUserUserIdView.as_view()), # チャットリスト取得get
    path('chat', ChatView.as_view()),# チャット投函post
    path('chat_message/_search/_chat/<str:chat_id>', ChatMessageSearchChatChatIdView.as_view()),# チャットメッセージリスト取得get
    path('chat_message', ChatMessageView.as_view()),# チャットメッセージ投函post
    path('chat_message_new_flag/_user/<str:user_id>', ChatMessageNewFlagUserUserIdView.as_view()), # ユーザの新しいチャットメッセージフラグ取得get
    path('chat_access', ChatAccessView.as_view()), # チャットアクセスリスト更新put
    path('chat_access/_search', ChatAccessSearchView.as_view()), # チャットアクセスリスト取得get
    # チャットアラートget
]
