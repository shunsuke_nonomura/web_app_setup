# from ...application_core import *
from application_core import *
from ..infrastructure_core import *

import copy

class MemoryProcessLogRepository(ProcessLogRepository):
    def post(self, process_log: ProcessLog):
        memory_process_log_list.append(process_log)
        return None
    def search(self):
        data_list = [d_i for d_i in reversed(memory_process_log_list)][:10]
        return copy.deepcopy(data_list)