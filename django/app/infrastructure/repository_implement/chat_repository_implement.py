# from ...application_core import *
from application_core import *
from ..infrastructure_core import *

import copy
class MemoryChatRepository(ChatRepository):
    def find_by_chat_id(self, chat_id: ChatId):
        data_list = memory_chat_list
        data_list = [data_i for data_i in data_list if chat_id == data_i.chat_id]
        return copy.deepcopy(data_list[0]) if len(data_list) > 0 else None
    def search_by_user_id(self, user_id: UserId):
        data_list = memory_chat_list
        # from pprint import pprint
        # pprint(data_list)
        data_list = [data_i for data_i in data_list if user_id in data_i.chat_user_id_list.__root__]
        data_list = sorted(data_list, key=lambda x: x.chat_update_datetime.__root__, reverse=True)
        return copy.deepcopy(data_list)
    def post(self, chat: Chat):
        memory_chat_list.append(chat)
        return None
    def update(self, chat: Chat):
        chat_id = chat.chat_id
        for i, data_i in enumerate(memory_chat_list):
            if data_i.chat_id == chat_id:
                memory_chat_list[i] = chat
        return None

class MemoryChatMessageRepository(ChatMessageRepository):
    def search_by_chat_id(self, chat_id: ChatId):
        data_list = memory_chat_message_list
        data_list = [data_i for data_i in data_list if data_i.chat_id == chat_id]
        return copy.deepcopy(data_list)
    def post(self, chat_message: ChatMessage):
        memory_chat_message_list.append(chat_message)
        # from pprint import pprint
        # pprint(memory_chat_message_list)
        return None

class MemoryChatAccessRepository(ChatAccessRepository):
    def search(self):
        data_list = memory_chat_access_list
        # print('*'*20)
        # print(data_list)
        data_list = sorted(data_list, key=lambda x: x.chat_access_update_datetime.__root__, reverse=True)
        return copy.deepcopy(data_list)
    def find_by_user_id(self, user_id: UserId):
        data_list = memory_chat_access_list
        data_list = [data_i for data_i in data_list if user_id == data_i.chat_access_user_id]
        return copy.deepcopy(data_list[0]) if len(data_list) > 0 else None
    def update(self, chat_access: ChatAccess):
        user_id = chat_access.chat_access_user_id
        for i, data_i in enumerate(memory_chat_access_list):
            if data_i.chat_access_user_id == user_id:
                memory_chat_access_list[i] = chat_access
        return None
    def post(self, chat_access: ChatAccess):
        memory_chat_access_list.append(chat_access)
        return None