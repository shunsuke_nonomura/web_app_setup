# from ...application_core import *
from application_core import *
from ..infrastructure_core import *

import copy

class MemoryUserRepository(UserRepository):
    def find_by_user_id(self, user_id: UserId):
        user_id_str = user_id.__root__
        if user_id_str not in memory_user_store_dict:
            return None
        return copy.deepcopy(memory_user_store_dict[user_id_str])
    
    def add_user(self, user: User):
        user_id = user.user_id
        memory_user_store_dict[user_id.__root__] = user
        return None
    
    def remove_user(self, user: User):
        user_id = user.user_id
        del memory_user_store_dict[user_id.__root__]
        return None
    
    def search(self):
        users = [i for i in memory_user_store_dict.values()]
        return copy.deepcopy(users)