from application_core import *
from ..infrastructure_core import *

import ulid
from datetime import datetime

class MemoryChatFactory(ChatFactory):
    def create(self, chat_title: str, chat_user_id_list: str = ''):
        current_datetime = datetime.now()
        chat = Chat(
            chat_id = ulid.new().str,
            chat_title = chat_title,
            chat_user_id_list = chat_user_id_list,
            chat_created_datetime = current_datetime,
            chat_update_datetime = current_datetime,
        )
        return chat

class MemoryChatMessageFactory(ChatMessageFactory):
    def create(self, chat_message_data: dict, chat_message_user_id: str, chat_id: str):
        chat_message_id = ulid.new().str
        chat_message_data_type = chat_message_data['chat_message_data_type']
        chat_message = None
        if chat_message_data_type == ChatMessageTypeEnum.Text.value:
            chat_message = ChatMessage(
                chat_message_id = chat_message_id,
                chat_message_data = ChatMessageDataText.parse_obj(chat_message_data),
                chat_message_user_id = chat_message_user_id,
                chat_message_created_datetime = datetime.now(),
                chat_id = chat_id,
            )
        # print(chat_message)
        return chat_message