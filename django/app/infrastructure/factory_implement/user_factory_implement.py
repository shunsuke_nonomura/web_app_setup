from application_core import *
from ..infrastructure_core import *

from datetime import datetime

class MemoryUserFactory(UserFactory):
    def create(self, user_id: str, user_name: str, user_password: str = '', user_role: str = ''):
        if user_password == '': # ない場合適当に作成
            user_password = pass_gen()
            # user_password = pass_hash_gen()
        user_created_datetime = datetime.now()
        user = User(
            user_id = user_id,
            user_password = user_password,
            user_name = user_name,
            user_role = user_role,
            user_created_datetime= user_created_datetime
        )
        return user