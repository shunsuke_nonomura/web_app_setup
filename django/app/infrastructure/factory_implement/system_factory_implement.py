from application_core import *
from ..infrastructure_core import *

import ulid
from datetime import datetime

class MemoryProcessLogFactory(ProcessLogFactory):
    def create(self, process_log_request: RequestForm, process_log_response: ResponseForm):
        process_log_id = ulid.new().str # 適当
        process_log = ProcessLog(
            process_log_id = process_log_id,
            process_log_request = process_log_request,
            process_log_response = process_log_response,
            process_log_created_datetime = datetime.now()
        )
        return process_log