import os
import sqlite3

class SqliteDriver():
    def __init__(self):
        self.database_name = 'tmp.db'
    def reset_db(self):
        print('on reset sqlite db')
        # DBを作成する
        if os.path.exists(self.database_name ):
            os.remove(self.database_name)
        conn = sqlite3.connect(self.database_name )
        # cur = conn.cursor() # SQLiteを操作するためのカーソルを作成
        # テーブルの作成
        # cur.execute(
        #     """
        #     CREATE TABLE items(
        #         id INTEGER PRIMARY KEY AUTOINCREMENT, 
        #         name STRING, 
        #         price INTEGER
        #     )
        #     """
        # )
        # DBとの接続を閉じる(必須)
        conn.close()
        return None
