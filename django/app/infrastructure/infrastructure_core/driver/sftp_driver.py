import paramiko
import json
from pprint import pprint

class SFTPDriver():
    def __init__(self, host, port, username, password):
        # SFTP接続先の設定
        # HOST = 'sftp-without-key'
        self.host = host
        self.port = port
        self.username = username
        self.password = password

    def connect(self):
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy)
        client.connect(
            self.host, 
            port=self.port, 
            username=self.username, 
            password=self.password
        )
        return client

    def sample(self):
        try:
            client = self.connect()
            # SFTPセッション開始
            sftp_connection = client.open_sftp()
            # ファイル一覧の取得
            entries = sftp_connection.listdir('./remote')
            entries = sorted(entries)
            pprint(entries)

            # ファイル取得
            with sftp_connection.open("./remote/{entries[-1]}/conf.json", "r") as f:
                json_load = json.load(f)
                pprint(json_load)
        finally:
            client.close()