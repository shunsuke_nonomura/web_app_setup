from .driver import *
from .memory import *
from .engine import *
from .task import *

class Infrastructure():
    pass
    # celery_driver = CeleryDriver()
    # sftp_driver = SFTPDriver(
    #     host = '192.168.10.10',
    #     port = 10022,
    #     username = 'sftp-without-key',
    #     password = 'pass',
    # )
    # sqlite_driver = SqliteDriver()
