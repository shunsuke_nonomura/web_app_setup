# 内部メモリ用保存領域
from application_core import *

import ulid
from datetime import datetime

# user
memory_user_store_dict = {}

for i in range(3):
    user_id = 'u' + str(i)
    memory_user_store_dict[user_id] = User(
        user_id = user_id,
        user_password = SecretStr('p'),
        user_name = 'n' + str(i),
        user_role = UserRoleEnum.General,
        user_created_datetime = datetime.now(),
    )

memory_user_store_dict['test'] = User(
    user_id = 'test',
    user_password = SecretStr('test'),
    user_name = 'test',
    user_role = UserRoleEnum.General,
    user_created_datetime = datetime.now()
)

admin_id = 'admin'
memory_user_store_dict[admin_id] = User(
    user_id = admin_id,
    user_password = SecretStr(admin_id),
    user_name = admin_id,
    user_role = UserRoleEnum.Admin,
    user_created_datetime = datetime.now()
)

# process
memory_process_log_list = []

# chat
memory_chat_created_datetime = datetime.now()
memory_chat_list = [
    Chat(
        chat_id = ulid.new().str,
        chat_title = 'hoge',
        chat_user_id_list = ['u0', 'u1'],
        chat_created_datetime= memory_chat_created_datetime,
        chat_update_datetime= memory_chat_created_datetime,
    ),
    Chat(
        chat_id = ulid.new().str,
        chat_title = 'hoge',
        chat_user_id_list = ['u0', 'u1'],
        chat_created_datetime= memory_chat_created_datetime,
        chat_update_datetime= memory_chat_created_datetime,
    ),
    Chat(
        chat_id = ulid.new().str,
        chat_title = 'hoge',
        chat_user_id_list = ['u1'],
        chat_created_datetime= memory_chat_created_datetime,
        chat_update_datetime= memory_chat_created_datetime,
    )
]

memory_chat_message_list = [
    ChatMessage(
        chat_message_id = ulid.new().str,
        chat_message_data = ChatMessageDataText(
            chat_message_data_text = 'hogehoge'
        ),
        chat_message_user_id = 'u0',
        chat_message_created_datetime = datetime.now(),
        chat_id = memory_chat_list[0].chat_id,
    ),
    ChatMessage(
        chat_message_id = ulid.new().str,
        chat_message_data = ChatMessageDataText(
            chat_message_data_text = 'hogehoge'
        ),
        chat_message_user_id = 'u0',
        chat_message_created_datetime = datetime.now(),
        chat_id = memory_chat_list[0].chat_id,
    ),
    ChatMessage(
        chat_message_id = ulid.new().str,
        chat_message_data = ChatMessageDataText(
            chat_message_data_text = 'hogehoge'
        ),
        chat_message_user_id = 'u0',
        chat_message_created_datetime = datetime.now(),
        chat_id = memory_chat_list[0].chat_id,
    ),
]

memory_chat_access_list = []