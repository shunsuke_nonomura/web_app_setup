import string
import secrets
import hashlib

def pass_gen(size=8):
   chars = string.ascii_uppercase + string.ascii_lowercase + string.digits
   # 記号を含める場合
   # chars += '%&$#()'
   return ''.join(secrets.choice(chars) for x in range(size))

def pass_hash_gen(size=8):
   password = pass_gen(size=size)
   return hashlib.sha256(password.encode()).hexdigest()