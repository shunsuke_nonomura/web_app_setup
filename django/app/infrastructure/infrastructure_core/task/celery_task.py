"""
自動起動のタスク管理用途
バックエンドに定期的に処理を行う用途
タスクの登録状況がredisに記録される
- heart beat
"""
import celery
from celery.schedules import crontab
import os
import requests

CELERY_BROKER = os.environ.get('CELERY_BROKER')
CELERY_BACKEND = os.environ.get('CELERY_BACKEND')

CELERY_RESULT_EXPIRES = 1

celery_app = celery.Celery(
    'task',
    broker=CELERY_BROKER,
    backend=CELERY_BACKEND,
)


# # タスクの結果の保持時間を変更
from datetime import timedelta
celery_app.conf.update(
    result_expires=timedelta(seconds=1),
    # task_result_expires=timedelta(seconds=1)
    # CELERY_TASK_RESULT_EXPIRES=10
    # CELERY_RESULT_EXPIRES=10
)
# result_expires=timedelta(seconds=1)

@celery_app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(
        10.0, # 10秒ごと
        heartbeat.s(), 
        name='heartbeat'
    )

@celery_app.task(
    ignore_result=True
)
def heartbeat():
    """
    バックエンドにheartbeatを実施。以下どちらかの場合は失敗なのでハンドリングが必要
    1. リクエスト自体が失敗する場合
    2. レスポンス内容が失敗の場合
    """
    url = 'http://django:8000/view/heartbeat'
    error = None
    try:
        r = requests.get(url)
    except Exception as e:
        error = e
    is_request_error = error is not None
    if is_request_error:
        print('on heartbeat error')
        raise Exception
    return error

@celery_app.task(
    result_expires=timedelta(seconds=1)
)
def sample():
    # https://stackoverflow.com/questions/12664250/celery-marks-all-output-as-warning/12670475
    # print('on a wait task in') # print使うとwarnilngになる模様
    import time
    time.sleep(3)
    # celery.task.backend_cleanup()
    # celery.task.backend_cleanup()
    return []