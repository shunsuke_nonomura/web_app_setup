from application_core import *
from ..infrastructure_core import *

import string
import secrets

class ResponseFormBuilderImplement(ResponseFormBuilder):
    def build(self, head: ResponseFormHead, data: dict, error: Exception):
        """
        { head, data }のResponseForm形式に整える
        """
        process_code = head.process_code # 処理番号 # processはわからない気がする？
        error_code = error.error_code if isinstance(error, ErrorCodeException) else ErrorCodeEnum.UndefinedError if error is not None else ErrorCodeEnum.NoError # 障害番号
        error_message = '' if error is None else str(error) # エラー時にエラーメッセージ
        result_code = ResultCodeEnum.SuccessCode if error is None else ResultCodeEnum.ErrorCode # S, E, (W)

        process_start_datetime = head.process_start_datetime
        process_end_datetime = datetime.now()
        response_form = ResponseForm(
            head = ResponseFormHead(
                response_code = result_code.value + process_code.__root__.value + error_code,
                error_code = error_code,
                error_message = error_message,
                process_code = process_code,
                process_name = head.process_name,
                process_start_datetime = process_start_datetime,
                process_end_datetime = process_end_datetime,
                process_dulation_timedelta = process_end_datetime - process_start_datetime,
            ), 
            data = data
        )
        return response_form