# from ...application_core import *
from application_core import *
from ..infrastructure_core import *

from pprint import pprint
import datetime

class TransferMessageServiceConsole(TransferMessageService):
    def transfer_text(self, text: Text):
        print(f"[{datetime.datetime.now()}] {text.__root__}")
    def transfer_response_form_head(self, head: ResponseFormHead):
        print(f"[{datetime.datetime.now()}] {head.process_name}")
        pprint(head.dict())