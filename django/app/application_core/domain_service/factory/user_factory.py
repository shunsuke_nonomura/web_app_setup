from abc import ABCMeta, abstractmethod
from ...domain_model import *

class UserFactory(metaclass=ABCMeta):
    @abstractmethod
    def create(self):
        raise NotImplementedError