from abc import ABCMeta, abstractmethod
from ...domain_model import *

class ProcessLogFactory(metaclass=ABCMeta):
    @abstractmethod
    def create(self):
        raise NotImplementedError