from abc import ABCMeta, abstractmethod
from ...domain_model import *

class ChatFactory(metaclass=ABCMeta):
    @abstractmethod
    def create(self):
        raise NotImplementedError

class ChatMessageFactory(metaclass=ABCMeta):
    @abstractmethod
    def create(self):
        raise NotImplementedError