from abc import ABCMeta, abstractmethod
from ...domain_model import *

class ProcessLogRepository(metaclass=ABCMeta):
    @abstractmethod
    def post(self):
        raise NotImplementedError
    @abstractmethod
    def search(self):
        raise NotImplementedError