from abc import ABCMeta, abstractmethod
from ...domain_model import *

class ChatRepository(metaclass=ABCMeta):
    @abstractmethod
    def search_by_user_id(self):
        raise NotImplementedError
    @abstractmethod
    def post(self):
        raise NotImplementedError

class ChatMessageRepository(metaclass=ABCMeta):
    @abstractmethod
    def search_by_chat_id(self):
        raise NotImplementedError
    @abstractmethod
    def post(self):
        raise NotImplementedError

class ChatAccessRepository(metaclass=ABCMeta):
    @abstractmethod
    def find_by_user_id(self):
        raise NotImplementedError
    @abstractmethod
    def update(self):
        raise NotImplementedError