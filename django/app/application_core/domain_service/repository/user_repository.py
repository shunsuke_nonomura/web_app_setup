from abc import ABCMeta, abstractmethod
from ...domain_model import *

class UserRepository(metaclass=ABCMeta):
    @abstractmethod
    def find_by_user_id(self):
        raise NotImplementedError
    @abstractmethod
    def add_user(self):
        raise NotImplementedError
    @abstractmethod
    def remove_user(self):
        raise NotImplementedError
    @abstractmethod
    def search(self):
        raise NotImplementedError

