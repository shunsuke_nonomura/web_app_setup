from abc import ABCMeta, abstractmethod
from ...domain_model import *

class ResponseFormBuilder(metaclass=ABCMeta):
    @abstractmethod
    def build(self):
        raise NotImplementedError