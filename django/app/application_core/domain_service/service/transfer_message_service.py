from abc import ABCMeta, abstractmethod
from ...domain_model import *

class TransferMessageService(metaclass=ABCMeta):
    @abstractmethod
    def transfer_text(self):
        raise NotImplementedError
    # @abstractmethod
    # def transfer_error(self):
    #     raise NotImplementedError