from .builder import *
from .factory import *
from .repository import *
from .service import *

"""
ApplicationServiceレイヤでドメインサービスレイヤ以下を自由に扱うためのクラス

ApplicationServiceレイヤでの共通処理
init_response : レスポンスデータの初期化
post_process_response : レスポンスデータの後処理
"""
import inject
class ApplicationService():
    # core
    response_form_builder = inject.attr(ResponseFormBuilder)
    task_management_service = inject.attr(TaskManagementService)
    transfer_message_service = inject.attr(TransferMessageService)

    # system
    process_log_repository = inject.attr(ProcessLogRepository)
    process_log_factory = inject.attr(ProcessLogFactory)

    # user
    user_repository = inject.attr(UserRepository)
    user_factory = inject.attr(UserFactory)

    # chat
    chat_repository = inject.attr(ChatRepository)
    chat_factory = inject.attr(ChatFactory)
    chat_message_repository = inject.attr(ChatMessageRepository)
    chat_message_factory = inject.attr(ChatMessageFactory)
    chat_access_repository = inject.attr(ChatAccessRepository)

    # 記録を無視するプロセス
    ignore_record_process_code_list = [
        ProcessCodeEnum.ProcessLogSearch, # ログを返したというログが作られることで再帰的にネストをつくるため記録しない
    ]

    # 問題監視するプロセス
    monitoring_problem_process_code_all = False # monitoring_problem_process_code_listにかかわらずすべて監視するかどうか
    monitoring_problem_process_code_list = [ # 問題監視するプロセス
        # ProcessCodeEnum.TaskSearch
    ]
    threshold_slow_second = 10 # 遅い処理のしきい値

    # レスポンスデータの初期化
    def init_response(self, process_code: ProcessCode):
        """
        headとdataとerrorの初期化
        ResponseFormHead, data(dict), error(Exception)
        """
        head = ResponseFormHead(
            process_code = process_code,
            process_name = process_code.name,
            process_start_datetime = datetime.now()
        )
        data = {}
        error = None
        return head, data, error

    # アプリケーションのポストプロセス
    def post_process_response(self, head: ResponseFormHead, data: Any, error):
        """
        1. response_formの生成
        2. 処理内容の記録
        3. 問題のあった処理の通知
        4. response_formの出力
        """
        # 1. response_formの生成
        response_form = self.response_form_builder.build(head = head, data = data, error = error)
        # 2. 処理内容の記録
        is_record_process = response_form.head.process_code.__root__ not in self.ignore_record_process_code_list # 記録対象外のプロセスではない
        if is_record_process:
            process_log = self.process_log_factory.create(
                process_log_request = RequestForm(), # 仮
                process_log_response = response_form
            )
            self.process_log_repository.post(process_log)
        # 3. 問題のあった処理の通知
        is_error = response_form.head.error_code.__root__ != ErrorCodeEnum.NoError.value # エラーがある
        is_takes_too_long = response_form.head.process_dulation_timedelta.total_seconds() >= self.threshold_slow_second # 時間がかかりすぎる
        is_monitoring_process = response_form.head.process_code.__root__ in self.monitoring_problem_process_code_list # 監視対象のプロセス
        if (is_error or is_takes_too_long) and (is_monitoring_process or self.monitoring_problem_process_code_all):
            self.transfer_message_service.transfer_response_form_head(response_form.head)
        # 4. response_formの出力
        return response_form
