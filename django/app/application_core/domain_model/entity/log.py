from pydantic import validator, BaseModel, SecretStr
from enum import Enum, IntEnum
from typing import Any
from datetime import datetime

from ..value_object import *

class ProcessLogId(ValueObject):
    __root__ : str

class ProcessLogCreatedDatetime(ValueObject):
    __root__ : datetime

class ProcessLog(Entity):
    process_log_id: ProcessLogId
    process_log_request: RequestForm
    process_log_response: ResponseForm
    process_log_created_datetime: ProcessLogCreatedDatetime

    def _id(self):
        return self.process_log_id

# class ProcessLogSummary(BaseModel):
#     process_log_id: ProcessLogId
#     process_log_created_datetime: ProcessLogCreatedDatetime