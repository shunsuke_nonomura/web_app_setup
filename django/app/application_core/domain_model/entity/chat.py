from pydantic import validator, BaseModel, SecretStr
from enum import Enum, IntEnum
from typing import Any, List
from datetime import datetime

from ..core import *
from ..value_object import *
from .user import *

class ChatId(Id):
    pass

class ChatTitle(Title):
    pass

class ChatCreatedDatetime(Datetime):
    pass

class ChatUpdateDatetime(Datetime):
    pass


class Chat(Entity):
    chat_id: ChatId # 紐付いているchat
    chat_title: ChatTitle # チャットタイトル
    chat_user_id_list: UserIdList # 参加者ID
    chat_created_datetime: ChatCreatedDatetime
    chat_update_datetime: ChatUpdateDatetime

    def _id(self):
        return self.chat_id

class ChatMessageData(ValueObject):
    chat_message_data_type: ChatMessageTypeEnum
class ChatMessageDataText(ChatMessageData):
    chat_message_data_type = ChatMessageTypeEnum.Text
    chat_message_data_text: str

    @validator("chat_message_data_text")
    def validate_chat_message_data_text(cls, v):
        if v == '':
            raise ValueError("NotArrowEmpty")
        return v

class ChatMessageId(Id):
    pass

class ChatMessage(Entity):
    chat_message_id: ChatMessageId
    chat_message_data: ChatMessageData
    chat_message_user_id: UserId
    chat_message_created_datetime: datetime
    chat_id: ChatId # 紐付いているchat

    def _id(self):
        return self.chat_message_id

class ChatAccessId(Id):
    pass
class ChatAccessCreatedDatetime(Datetime):
    pass
class ChatAccessUpdateDatetime(Datetime):
    pass

class ChatAccess(Entity):
    chat_access_id: ChatAccessId
    chat_access_user_id: UserId
    chat_access_created_datetime: ChatAccessCreatedDatetime
    chat_access_update_datetime: ChatAccessUpdateDatetime

    def _id(self):
        return self.chat_access_id
