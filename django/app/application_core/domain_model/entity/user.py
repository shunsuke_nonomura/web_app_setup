from pydantic import validator, BaseModel, SecretStr
from enum import Enum, IntEnum
from typing import Any, List
from datetime import datetime

from ..core import *
from ..value_object import *

class UserId(Id):
    pass
class UserIdList(ValueObject):
    __root__: List[UserId]

class UserPassword(Password):
    pass

class UserName(Name):
    pass

class UserRole(ValueObject):
    __root__ : UserRoleEnum

class UserCreatedDatetime(Datetime):
    pass

class User(Entity):
    user_id: UserId
    user_password: UserPassword
    user_name: UserName
    user_role: UserRole
    user_created_datetime: UserCreatedDatetime

    def _id(self):
        return self.user_id
    
    def auth_password(self, user_password: UserPassword): # 自分自身のパスワードと入力パスワードが等しいかどうか
        if user_password != self.user_password:
            return False
        return True