from pydantic import validator, BaseModel, SecretStr
from enum import Enum, IntEnum, unique
from typing import Any, Optional
from ..core import *
from datetime import datetime, timedelta

class Id(ValueObject):
    __root__ : str
    @validator("__root__")
    def validate_root(cls, v):
        if v == '':
            raise ValueError("NotArrowEmpty")
        return v

class Password(ValueObject):
    __root__ : SecretStr

class Name(ValueObject):
    __root__ : str
    @validator("__root__")
    def validate_root(cls, v):
        if v == '':
            raise ValueError("NotArrowEmpty")
        return v

class Title(ValueObject):
    __root__ : str
    @validator("__root__")
    def validate_root(cls, v):
        if v == '':
            raise ValueError("NotArrowEmpty")
        return v

class Text(ValueObject):
    __root__ : str

class Number(ValueObject):
    __root__ : int

class Datetime(ValueObject):
    __root__ : datetime
