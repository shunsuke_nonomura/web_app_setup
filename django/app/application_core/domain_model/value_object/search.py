from pydantic import validator, BaseModel, SecretStr
from enum import Enum, IntEnum, unique
from typing import Any, Optional, List
from ..core import *
from .common import *

from datetime import datetime, timedelta

class FilterKey(ValueObject):
    __root__: str

class Length(ValueObject):
    __root__: int

class SearchCommand(ValueObject):
    filter_list : List[FilterKey]
    result_length: Length
    from_number: Number