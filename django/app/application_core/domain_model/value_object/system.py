from pydantic import validator, BaseModel, SecretStr
from enum import Enum, IntEnum, unique
from typing import Any, Optional
from ..core import *
from datetime import datetime, timedelta

class ProcessCode(ValueObject):
    __root__: ProcessCodeEnum

# リクエスト
class RequestFormHead(ValueObject):
    process_code: ProcessCode = ProcessCodeEnum.Undefined
    process_name: str = ''

class RequestForm(ValueObject):
    head: RequestFormHead = RequestFormHead()
    data: Any = {}

# レスポンス
class ResponseFormHead(ValueObject):
    response_code: str = ''
    error_code: ErrorCode = ErrorCodeEnum.NoError
    error_message: str = ''
    process_code: ProcessCode = ProcessCodeEnum.Undefined
    process_name: str = ''
    process_start_datetime: datetime = datetime.now() # 起動時の固定値になる
    process_end_datetime: Optional[datetime] = None
    process_dulation_timedelta: timedelta = 0

class ResponseForm(ValueObject):
    head: ResponseFormHead = ResponseFormHead()
    data: Any = {}