from enum import Enum, IntEnum, unique

@unique
class ProcessCodeEnum(str, Enum):
    # System
    Heartbeat = "0000"
    ProcessLogSearch = "0100"
    TaskSearch = "0200"
    # User
    UserFindByUserId = "1000"
    UserSearch = "1001"
    UserLogin = "1010"
    UserSignUp = "1011"
    UserDelete = "1030"
    # Chat
    ChatSearchByUserId = "2000"
    ChatPostChat = "2010"
    ChatMessageSearchByChatId = "2100"
    ChatMessagePostChatMessage = "2110"
    ChatMessageNewFlagCheckByUserId = "2200"
    ChatAccessSearch = "2300"
    ChatAccessUpdate = "2310"

    # Other
    Undefined = "9999"

@unique
class ErrorCodeEnum(str, Enum):
    # HeartbeatError = '001'
    UserNotFoundError = '100'
    UserDuplicationError = '101'
    UserAuthenticationError = '102'
    
    UndefinedError = '999'
    NoError = '000'

@unique
class ResultCodeEnum(str, Enum):
    SuccessCode = 'S'
    ErrorCode = 'E'
    WarningCode = 'W'

@unique
class UserRoleEnum(Enum):
    Admin = '00'
    General = '10'
    Guest = '99'

@unique
class ChatMessageTypeEnum(Enum):
    Text = '00'
    Undefined = '99'