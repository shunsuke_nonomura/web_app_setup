from pydantic import BaseModel
from abc import ABCMeta, abstractmethod

class ValueObject(BaseModel, metaclass=ABCMeta):
    class Config:
        allow_mutation = False


class Entity(BaseModel, metaclass=ABCMeta):
    @abstractmethod
    def _id(self): # IDの取得方法を必ず設計する
        raise NotImplementedError

    # IDで比較するロジック
    def __eq__(self, other):
        if other is None or type(self) != type(other): return False # isinstance(other, Entity)を除去
        return self._id() == other._id()
    def __ne__(self, other):
        return not self.__eq__(other)