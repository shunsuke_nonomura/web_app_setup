from .core_ddd import *
from .core_enum import *

class ErrorCode(ValueObject):
    __root__: ErrorCodeEnum

class ErrorCodeException(Exception):
    error_code: ErrorCode = ErrorCodeEnum.UndefinedError # デフォルト値はUndefined
    def __str__(self):
        return self.error_code.name

# # system
# class HeartbeatError(ErrorCodeException):
#     error_code = ErrorCodeEnum.HeartbeatError

# user
class UserNotFoundError(ErrorCodeException):
    error_code: ErrorCode = ErrorCodeEnum.UserNotFoundError

class UserDuplicationError(ErrorCodeException):
    error_code: ErrorCode = ErrorCodeEnum.UserDuplicationError

class UserAuthenticationError(ErrorCodeException):
    error_code: ErrorCode = ErrorCodeEnum.UserAuthenticationError