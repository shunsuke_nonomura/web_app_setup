from abc import ABCMeta, abstractmethod
from ..domain_model import *
from ..domain_service import *

import json

class ChatService(ApplicationService):
    def chat_search_by_user_id(self, user_id: str):
        head, data, error = self.init_response(process_code = ProcessCodeEnum.ChatSearchByUserId)
        try:
            user_id = UserId(__root__=user_id)
            chat_list = self.chat_repository.search_by_user_id(user_id = user_id)
            data = {'chat_list': [json.loads(d_i.json()) for d_i in chat_list]}
        except Exception as e:
            error = e
        return self.post_process_response(head = head, data = data, error = error)
    def chat_post(self, chat_title, chat_user_id_list):
        head, data, error = self.init_response(process_code = ProcessCodeEnum.ChatPostChat)
        try:
            chat = self.chat_factory.create(chat_title, chat_user_id_list)
            self.chat_repository.post(chat)
        except Exception as e:
            error = e
        return self.post_process_response(head = head, data = data, error = error)
    def chat_message_search_by_chat_id(self, chat_id: str):
        head, data, error = self.init_response(process_code = ProcessCodeEnum.ChatMessageSearchByChatId)
        try:
            chat_id = ChatId(__root__=chat_id)
            chat_message_list = self.chat_message_repository.search_by_chat_id(chat_id = chat_id)
            data = {'chat_message_list': [json.loads(d_i.json()) for d_i in chat_message_list]}
        except Exception as e:
            error = e
        return self.post_process_response(head = head, data = data, error = error)
    def chat_message_post(self, chat_message_data, chat_message_user_id, chat_id):
        head, data, error = self.init_response(process_code = ProcessCodeEnum.ChatMessagePostChatMessage)
        try:
            chat_id = ChatId(__root__=chat_id)
            chat_message = self.chat_message_factory.create(chat_message_data, chat_message_user_id, chat_id)
            self.chat_message_repository.post(chat_message)
            chat = self.chat_repository.find_by_chat_id(chat_id)
            chat.chat_update_datetime = ChatUpdateDatetime(__root__=datetime.now())
            self.chat_repository.update(chat)
        except Exception as e:
            error = e
        return self.post_process_response(head = head, data = data, error = error)
    
    def chat_message_new_flag_check_by_user_id(self, user_id):
        head, data, error = self.init_response(process_code = ProcessCodeEnum.ChatMessageNewFlagCheckByUserId)
        try:
            pass
        except Exception as e:
            error = e
        return self.post_process_response(head = head, data = data, error = error)
    
    def chat_access_search(self):
        head, data, error = self.init_response(process_code = ProcessCodeEnum.ChatAccessSearch)
        try:
            chat_access_list = self.chat_access_repository.search()
            data = {'chat_access_list': [json.loads(d_i.json()) for d_i in chat_access_list]}
        except Exception as e:
            error = e
        return self.post_process_response(head = head, data = data, error = error)

    def chat_access_update_by_user_id(self, user_id):
        head, data, error = self.init_response(process_code = ProcessCodeEnum.ChatAccessUpdate)
        try:
            chat_access = self.chat_access_repository.find_by_user_id(user_id)
            chat_access_datetime = datetime.now()
            if chat_access is None: # ない場合追加
                chat_access = ChatAccess(
                    chat_access_id = 'hoge',
                    chat_access_user_id = user_id,
                    chat_access_created_datetime = chat_access_datetime,
                    chat_access_update_datetime = chat_access_datetime,
                )
                self.chat_access_repository.post(chat_access)
            else: # ある場合更新
                chat_access.chat_access_update_datetime = chat_access_datetime
                self.chat_access_repository.update(chat_access)
        except Exception as e:
            error = e
        return self.post_process_response(head = head, data = data, error = error)
