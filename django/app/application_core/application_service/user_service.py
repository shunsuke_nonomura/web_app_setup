from abc import ABCMeta, abstractmethod
from ..domain_model import *
from ..domain_service import *

# class UserService(metaclass=ABCMeta):
#     @abstractmethod
#     def login(self):
#         raise NotImplementedError
#     @abstractmethod
#     def create(self)
#         raise NotImplementedError

import json

class UserService(ApplicationService):
    def find_by_user_id(self, user_id: UserId):
        head, data, error = self.init_response(process_code = ProcessCodeEnum.UserFindByUserId)
        try:
            # user_id = UserId(__root__=user_id)
            user = self.user_repository.find_by_user_id(user_id)
            if user is None:
                raise UserNotFoundError
            data = {'user': json.loads(user.json())}
        except Exception as e:
            error = e
        return self.post_process_response(head = head, data = data, error = error)
    def search(self):
        head, data, error = self.init_response(process_code = ProcessCodeEnum.UserSearch)
        try:
            user_list = self.user_repository.search()
            data = {'user_list': [json.loads(user.json()) for user in user_list]}
        except Exception as e:
            error = e
        return self.post_process_response(head = head, data = data, error = error)
    def login(self, user_id: str, user_password: SecretStr):
        head, data, error = self.init_response(process_code = ProcessCodeEnum.UserLogin)
        try:
            user_id = UserId(__root__=user_id)
            user_password = UserPassword(__root__=user_password)
            user = self.user_repository.find_by_user_id(user_id)
            if user is None:
                raise UserNotFoundError
            is_auth = user.auth_password(user_password)
            if is_auth is False:
                raise UserAuthenticationError
        except Exception as e:
            error = e
        return self.post_process_response(head = head, data = data, error = error)
    def sing_up(self, user_id, user_name, user_password = '', user_role = UserRoleEnum.General):
        head, data, error = self.init_response(process_code = ProcessCodeEnum.UserSignUp) 
        try:
            user_id = UserId(__root__=user_id)
            user = self.user_repository.find_by_user_id(user_id)
            if user is not None:
                raise UserDuplicationError
            user = self.user_factory.create(
                user_id = user_id, 
                user_password = user_password, 
                user_name = user_name,
                user_role = user_role
            )
            self.user_repository.add_user(user)
        except Exception as e:
            error = e
        return self.post_process_response(head = head, data = data, error = error)
    def delete(self, user_id: str):
        head, data, error = self.init_response(process_code = ProcessCodeEnum.UserDelete)
        try:
            user_id = UserId(__root__=user_id)
            user = self.user_repository.find_by_user_id(user_id)
            if user is None:
                raise UserNotFoundError
            self.user_repository.remove_user(user)
        except Exception as e:
            error = e
        return self.post_process_response(head = head, data = data, error = error)
    
    