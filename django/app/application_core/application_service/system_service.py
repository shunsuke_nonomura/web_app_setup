from abc import ABCMeta, abstractmethod
from ..domain_model import *
from ..domain_service import *

# class SystemService(metaclass=ABCMeta):
#     @abstractmethod
#     def login(self):
#         raise NotImplementedError
#     @abstractmethod
#     def create(self)
#         raise NotImplementedError

import json

class SystemService(ApplicationService):
    def heartbeat(self):
        head, data, error = self.init_response(process_code = ProcessCodeEnum.Heartbeat)
        try:
            pass
        except Exception as e:
            error = e
        return self.post_process_response(head = head, data = data, error = error)
    def process_log_search(self):
        head, data, error = self.init_response(process_code = ProcessCodeEnum.ProcessLogSearch)
        try:
            process_log_list = self.process_log_repository.search()
            data = {'process_log_list': [json.loads(d_i.json()) for d_i in process_log_list]}
        except Exception as e:
            error = e
        return self.post_process_response(head = head, data = data, error = error)
    def task_search(self):
        head, data, error = self.init_response(process_code = ProcessCodeEnum.TaskSearch)
        try:
            self.task_management_service.sample()
            
        except Exception as e:
            error = e
        return self.post_process_response(head = head, data = data, error = error)
