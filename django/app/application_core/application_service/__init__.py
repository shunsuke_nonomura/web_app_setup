# appレイヤ (基本的にdomainの応用にしかならないから、DIしなくても良いかもしれない)
# from ..application_service import *
from .system_service import *
from .user_service import *
from .chat_service import *


from ..domain_model import *
from ..domain_service import *

from rest_framework.response import Response
from datetime import datetime

"""
UserInterfaceレイヤでApplicationレイヤを自由に扱うためのクラス
レスポンスのエラーコードもこの中でまとめる
"""
import inject
class UserInterface(ApplicationService):
    system_service = SystemService()
    user_service = UserService()
    chat_service = ChatService()
    